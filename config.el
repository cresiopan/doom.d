;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;;; Code:
;; Place your private configuration here
(load! "bindings")
(load! "functions")

(put 'find-alternate-file 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'capitalize-region 'disabled nil)

;; focus follows mouse
(setq mouse-autoselect-window t
      focus-follows-mouse t)

;; set font
(setq doom-font (font-spec :family "Source Code Pro" :size 15))

;; set backup directory
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

(provide 'config)
;;; config.el ends here
