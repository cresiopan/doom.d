;;; package --- Summary
;;; Commentary:

;;; Code:
;; taken from ~/.emacs.d/modules/config/default/+evil-bindings.el
(map!
 ;; Global evil keybinds
 :n  "gp"    nil
 :nv "g="    nil
 :nv "g-"    nil
 :v  "g+"    nil
 :v  "gp"    nil
 :v  "@"     nil

 ;; window management (prefix "C-w")
 (:map evil-window-map
   ;; Navigation
   "C-j"     #'evil-window-left
   "C-k"     #'evil-window-down
   "C-l"     #'evil-window-up
   "C--"     #'evil-window-right
   "C-w"     #'other-window
   ;; Swapping windows
   "J"       #'+evil/window-move-left
   "K"       #'+evil/window-move-down
   "L"       #'+evil/window-move-up
   "_"       #'+evil/window-move-right
   "C-S-w"   #'ace-swap-window
   ;; Window undo/redo
   "u"       #'winner-undo
   "C-r"     #'winner-redo
   "o"       #'doom/window-enlargen
   ;; Delete window
   "c"       #'+workspace/close-window-or-workspace
   "C-C"     #'ace-delete-window))

;;; Module keybinds

;;; :completion
(map! (:when (featurep! :completion company)
        :i "C-@"      nil
        (:prefix "C-x"
          :i "C-k"    nil
          :i "C-]"    nil
          :i "s"      nil
          :i "C-o"    nil
          :i "C-n"    nil
          :i "C-p"    nil)
        (:after company
          (:map company-active-map
            "C-w"     nil))))

;;; <leader>
(map! :leader

      :n "k" #'kill-current-buffer

      ;;; <leader> TAB --- workspace
      (:when (featurep! :ui workspaces)
        (:prefix-map ("TAB" . "workspace")
          :n "5" nil
          :n "6" nil
          :n "7" nil
          :n "8" nil
          :n "9" nil
          :n "0" nil
          :n "`" nil
          :n "x" nil
          :n "l" nil
          :n "s" nil
          :n "r" nil
          :n "R" nil))

      ;;; <leader> b --- buffer
      (:prefix-map ("b" . "buffer")
        :n "-" nil
        :n "[" nil
        :n "]" nil
        :n "o" nil
        :n "x" nil
        :n "X" nil
        :n "z" nil)

      ;;; <leader> c --- code
      (:prefix-map ("c" . "code")
        :n "l" #'display-line-numbers-mode
        :n "c" nil
        :n "r" nil
        :n "w" nil
        :n "W" nil
        :n "x" nil)

      ;;; <leader> f --- file
      (:prefix-map ("f" . "file")
        :n "c" nil
        :n "d" nil
        :n "e" nil
        :n "E" nil
        :n "f" nil
        :n "p" nil
        :n "P" nil
        :n "r" nil
        :n "R" nil)

      ;;; <leader> g --- git
      (:prefix-map ("g" . "git")
        :desc "Git revert file"             "R"   #'vc-revert
        (:when (featurep! :ui vc-gutter)
          :desc "Git revert hunk"           "r"   #'git-gutter:revert-hunk
          :desc "Git stage hunk"            "s"   #'git-gutter:stage-hunk
          :desc "Git time machine"          "t"   #'git-timemachine-toggle
          :desc "Jump to next hunk"         "]"   #'git-gutter:next-hunk
          :desc "Jump to previous hunk"     "["   #'git-gutter:previous-hunk)
        (:when (featurep! :tools magit)
          :desc "Magit dispatch"            "/"   #'magit-dispatch
          :desc "Forge dispatch"            "'"   #'forge-dispatch
          :desc "Magit status"              "g"   #'magit-status
          :desc "Magit file delete"         "x"   #'magit-file-delete
          :desc "Magit blame"               "B"   #'magit-blame-addition
          :desc "Magit clone"               "C"   #'+magit/clone
          :desc "Magit fetch"               "F"   #'magit-fetch
          :desc "Magit buffer log"          "L"   #'magit-log
          :desc "Git stage file"            "S"   #'magit-stage-file
          :desc "Git unstage file"          "U"   #'magit-unstage-file
          (:prefix ("f" . "find")
            :desc "Find file"                 "f"   #'magit-find-file
            :desc "Find gitconfig file"       "g"   #'magit-find-git-config-file
            :desc "Find commit"               "c"   #'magit-show-commit
            :desc "Find issue"                "i"   #'forge-visit-issue
            :desc "Find pull request"         "p"   #'forge-visit-pullreq)
          (:prefix ("o" . "open in browser")
            :desc "Browse region or line"     "."   #'+vc/git-browse-region-or-line
            :desc "Browse remote"             "r"   #'forge-browse-remote
            :desc "Browse commit"             "c"   #'forge-browse-commit
            :desc "Browse an issue"           "i"   #'forge-browse-issue
            :desc "Browse a pull request"     "p"   #'forge-browse-pullreq
            :desc "Browse issues"             "I"   #'forge-browse-issues
            :desc "Browse pull requests"      "P"   #'forge-browse-pullreqs)
          (:prefix ("l" . "list")
            (:when (featurep! :tools gist)
              :desc "List gists"              "g"   #'+gist:list)
            :desc "List repositories"         "r"   #'magit-list-repositories
            :desc "List submodules"           "s"   #'magit-list-submodules
            :desc "List issues"               "i"   #'forge-list-issues
            :desc "List pull requests"        "p"   #'forge-list-pullreqs
            :desc "List notifications"        "n"   #'forge-list-notifications)
          (:prefix ("c" . "create")
            :desc "Initialize repo"           "r"   #'magit-init
            :desc "Clone repo"                "R"   #'+magit/clone
            :desc "Commit"                    "c"   #'magit-commit-create
            :desc "Issue"                     "i"   #'forge-create-issue
            :desc "Pull request"              "p"   #'forge-create-pullreq)))

      ;;; <leader> n --- notes
      (:prefix-map ("n" . "notes")
        :desc "Browse notes"                  "." nil
        :desc "Open deft"                     "d" nil
        :desc "Search org agenda headlines"   "h" nil
        :desc "Org store link"                "l" nil)

      ;;; <leader> o --- open
      (:prefix-map ("o" . "open")
        :n "b" nil
        :n "d" nil
        :n "r" nil
        :n "R" nil)

      ;;; <leader> p --- project
      (:prefix-map ("p" . "project")
        :n "b" nil
        :n "!" nil
        :n "e" nil
        :n "i" nil
        :n "k" nil
        :n "r" nil
        :n "t" nil)

      ;;; <leader> r --- remote
      (:when (featurep! :tools upload)
        (:prefix-map ("r" . "remote")
          :n "u" nil
          :n "U" nil
          :n "d" nil
          :n "D" nil
          :n "." nil
          :n ">" nil))

      ;;; <leader> s --- snippets
      (:when (featurep! :editor snippets)
        (:prefix-map ("s" . "snippets")
          :n "S" nil
          :n "r" nil))

      ;;; <leader> t --- toggle
      (:prefix-map ("t" . "toggle")
        :desc "Treemacs" "t" #'treemacs
        :n               "F" nil
        :n               "i" nil
        :n               "h" nil
        :n               "b" nil
        :n               "g" nil
        :n               "p" nil))

(map! (:map evil-treemacs-state-map
        :nv "j" #'treemacs-root-up
        :nv "k" #'treemacs-next-line
        :nv "l" #'treemacs-previous-line
        :nv "-" #'treemacs-root-down)

      (:map treemacs-mode-map
        :nv "j" #'treemacs-root-up
        :nv "k" #'treemacs-next-line
        :nv "l" #'treemacs-previous-line
        :nv "-" #'treemacs-root-down
        :nv "h" #'treemacs-toggle-show-dotfiles)

      (:map help-map
        "\C-a" nil
        "\C-c" nil
        "\C-d" nil
        "\C-e" nil
        "\C-f" nil
        "\C-m" nil
        "\C-n" nil
        "\C-o" nil
        "\C-p" nil
        "\C-t" nil
        "\C-w" nil
        "\C-k" nil
        "\C-l" nil
        "\C-\\" nil
        "q"    nil
        "w"    nil
        "<f1>" nil
        "."    nil
        "?"    nil
        "B"    nil
        "D"    nil
        "E"    nil
        "O"    nil
        "P"    nil
        "T"    nil
        "V"    nil
        "W"    nil
        "F"    nil
        "K"    nil
        "S"    nil
        "g"    nil
        "4i"   nil
        "n"    nil
        "r"    nil
        "t"    nil))

;;(evil-define-key 'treemacs treemacs-mode-map (kbd "l") #'treemacs-previous-line)
(evil-define-key 'treemacs treemacs-mode-map (kbd "h") #'treemacs-toggle-show-dotfiles)

(map!
 :n "s-q" #'delete-window
 :n "s-c" #'kill-current-buffer
 :nvi "C-<backspace>"   #'cresiopan-backward-kill-char-or-word
 :nvi "C-M-<backspace>" #'cresiopan-kill-whole-line
 :nvi "<f12>"           #'cresiopan-fix-text

 ;; general navigation
 :nv "j" #'evil-backward-char
 :nv "k" #'evil-next-line
 :nv "l" #'evil-previous-line
 :nv "-" #'evil-forward-char
 :i  "C-j" #'backward-char
 :i  "C-k" #'forward-line
 :i  "C-l" #'previous-line
 :i  "C--" #'forward-char
 :i  "C-p" #'recenter-top-bottom
 :i  "M-j" #'backward-word
 :i  "M-k" #'forward-paragraph
 :i  "M-l" #'backward-paragraph
 :i  "M--" #'forward-word

 ;; bookmarks
 "C-c b" #'bookmark-jump

 ;; multiple cursors
 :i "C-<" #'mc/mark-next-like-this
 :i "C->" #'mc/mark-previous-like-this

 ;; other window
 "<C-tab>" (lambda () (interactive (other-window 1)))

 ;; auto yasnippet
 "C-c a c" #'aya-create
 "C-c a e" #'aya-expand

 ;; disabling some things
 "C-z" nil
 "M-z" nil
 "C-t" nil
 )

;; custom function bindings


;; duplicate line with C-c C-d
(global-set-key "\C-c\C-d" "\C-a\C- \C-e\M-w\C-o\C-y  ")

(provide 'bindings)
;;; bindings.el ends here
