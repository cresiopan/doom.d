;;; ~/.doom.d/functions.el -*- lexical-binding: t; -*-

;;; Code:
(defun xah-new-empty-buffer ()
  "Create a new empty buffer.
New buffer will be named “untitled” or “untitled<2>”, etc."
  (interactive)
  (let (($buf (generate-new-buffer "untitled")))
    (switch-to-buffer $buf)
    (funcall initial-major-mode)
    (setq-default buffer-offer-save t)
    $buf))

(defun xah-user-buffer-q ()
  "Return t if current buffer is a user buffer, else nil.
Typically, if buffer name starts with *, it's not considered a
user buffer. This function is used by buffer switching command
and close buffer command, so that next buffer shown is a user
buffer. You can override this function to get your idea of “user
buffer”."
  (interactive)
  (if (string-equal "*" (substring (buffer-name) 0 1))
      nil
      (if (string-equal major-mode "dired-mode")
          nil
          t)))

(defun xah-next-user-buffer ()
  "Switch to the next user buffer.
“user buffer” is determined by `xah-user-buffer-q'."
  (interactive)
  (next-buffer)
  (let ((i 0))
    (while (< i 20)
      (if (not (xah-user-buffer-q))
          (progn (next-buffer)
                 (setq i (1+ i)))
          (progn (setq i 100))))))

(defun xah-previous-user-buffer ()
  "Switch to the previous user buffer.
“user buffer” is determined by `xah-user-buffer-q'."
  (interactive)
  (previous-buffer)
  (let ((i 0))
    (while (< i 20)
      (if (not (xah-user-buffer-q))
          (progn (previous-buffer)
                 (setq i (1+ i)))
          (progn (setq i 100))))))

(defun cresiopan-open-line-below-and-switch ()
  "Open a line down from point and switch to it."
  (interactive)
  (progn
    (end-of-line)
    (open-line 1)
    (next-line)
    (indent-for-tab-command)))

(defun cresiopan-open-line-up-and-switch ()
  "Opens a new line up from point and switch to it."
  (interactive)
  (progn
    (beginning-of-line)
    (open-line 1)))

(defun cresiopan-backward-kill-char-or-word ()
  "Replacement for backward kill char or word ."
  (interactive)
  (cond
   ((looking-back (rx (char word)) 1)
    (backward-kill-word 1))
   ((looking-back (rx (char blank)) 1)
    (delete-horizontal-space t))
   (t (backward-delete-char 1))))

(defun cresiopan-kill-whole-line ()
  "Works like 'kill-whole-line' but does not kill the new line."
  (interactive)
  (progn
    (beginning-of-line)
    (let ((beg (point))) (end-of-line) (kill-region beg (point)))))

(defun cresiopan-open-terminal-in-new-window ()
  "Open term in a new buffer."
  (interactive)
  (cresiopan-make-split)
  (multi-term))

(defun cresiopan-open-new-empty-buffer-in-new-window ()
  (interactive)
  (cresiopan-make-split)
  (xah-new-empty-buffer))

(defun cresiopan-make-split ()
  "Split window vertically or horizontally depending on which is greater."
  (interactive)
  (if (> (window-pixel-width) (window-pixel-height))
      (split-window-horizontally)
      (split-window-vertically)))

(defun cresiopan-match-paren (ARG)
  "Go to the matching paren if on a paren; otherwise insert %."
  (interactive "p")
  (cond ((looking-at "\\s(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s)") (forward-char 1) (backward-list 1))
        (t (self-insert-command (or ARG 1)))))

(defun cresiopan-fix-text ()
  "Delete trailing whitespace, indent with spaces the whole buffer."
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max))
  (untabify (point-min) (point-max)))

(defun toggle-fold ()
  (interactive)
  (save-excursion
    (end-of-line)
    (hs-toggle-hiding)))

(defun dired-dotfiles-toggle ()
  "Show/hide dot-files."
  (interactive)
  (when (equal major-mode 'dired-mode)
    (if (or (not (boundp 'dired-dotfiles-show-p)) dired-dotfiles-show-p) ; if currently showing
        (progn (set (make-local-variable 'dired-dotfiles-show-p) nil)
               (message "h")
               (dired-mark-files-regexp "^\\\.")
               (dired-do-kill-lines))
      (progn (revert-buffer) ; otherwise just revert to re-show
             (set (make-local-variable 'dired-dotfiles-show-p) t)))))

(eval-after-load "dired"
  '(progn
     (define-key dired-mode-map (kbd "C-c C-h") 'dired-dotfiles-toggle)))

(provide 'functions)
;;; functions.el ends here
